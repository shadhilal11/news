

import './App.css'

function App() {


  return (
    <>
       <header>
      <div className="container">
        <span id="logo">NewsUpdate</span>
      
      <div className="menu">
          <nav>
            <ul className='first'   >
              <li>Home</li>
              <li>News</li>
              <li>About</li>
              <li>Contact</li>
            </ul>
          </nav>
      </div>
      <div className="menu22">
      <span class="material-symbols-outlined">
         menu_book
       </span>
      </div>
      </div>
    </header>

           <main>
           <section id='s1'>

<div id='container2' className="container2">
      <div id='firstnews' className="firstnews">
            
      </div>
      <div id='othernews' className="othernews">
      

      </div>
</div>

</section>




<section className='s2'>

     <h2>Other News</h2>


      <div id="maincontent"    className="maincontent">
    
      </div>


</section>
<section  className='s3'>
<div className="bitcoin">
  
</div>
</section>
  
           </main>
           <footer>
            <div className="containerfoot">
              <div className="about">
              <h5>About Us</h5>
              <p>NewsUpdate is your go-to source for the latest and most reliable news from India. Our dedicated team works around the clock to bring you accurate and timely updates on politics, business, technology, sports, entertainment, and more.</p>
              </div>
              <div className="contact">
                <h5>Contact us</h5>
                  <ul className='last'  >
                    <li> Email: contact@newsupdate.in</li>
                    <li>Phone: +91-123-456-7890</li>
                    <li>Address: 123 News Street, New Delhi, India</li>
                  </ul>
               </div>
               <div className="discli">
                <h5>Disclaimer</h5>
                <p>The content provided on NewsUpdate is for informational purposes only. While we strive for accuracy, we cannot guarantee the completeness or timeliness of the information presented.</p>
                <span className="copyright">
                © 2024 NewsUpdate. All Rights Reserved.
                </span>
               </div>

            </div>
           </footer>
    
    </>
  )
}

export default App
